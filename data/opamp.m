%Test Input Voltage level
Vin = [0, 0.01, 0.05, 0.08, 0.10,...
       0.20, 0.30, 0.50, 0.80, 1,...
       1.5, 2, 2.5, 3, 3.5];

% TL062 Data
Vo_TL062 = [28.57, 28.28, 19.01, 8.07, 2.425,...
            1.637, 1.982, 3.040, 7.25, 10.19,...
            16.57, 22.13, 27.63, 28.76, 28.76];
            
% NE5532 Data
Vo_NE5532 = [4.632, 4.610, 4.605, 4.605, 4.452,...
             4.589, 4.588, 5.527, 9.01, 11.15,...
             17.24, 23.64, 25.87, 25.90, 26.14];
             
%LM358 No Load Data
Vo_LM358_NL = [0, 0.129, 0.572, 0.901, 1.122,...
               2.23, 3.34, 5.55, 8.87, 11.09,...
               16.62, 22.14, 27.65, 28.52, 28.52];
            
%LM358 10K Ohms Data
Vo_LM358_L = [0.005, 0.131, 0.574, 0.902, 1.123,...
              2.23, 3.34, 5.55, 8.87, 11.08,...
              16.61, 22.13, 27.55, 28.45, 28.44];
 
%Ideal gain

Vo_MC34074 = [0.176, 0.168, 0.568, 0.896, 1.12,...
              2.22, 3.324, 5.54, 8.88, 11.03,...
              16.50, 22.2, 27.64, 29.00, 29.00];
Vo_ideal = 11*Vin;
 
%Plot Data 
figure(1);
hold on;
grid on;
plot(Vin, Vo_TL062, 'r-');
plot(Vin, Vo_MC34074, 'g-');
plot(Vin, Vo_NE5532, 'c-');
plot(Vin, Vo_LM358_NL, 'b-');
plot(Vin, Vo_ideal, 'k-');
xlabel('Vin');
ylabel('Vout');
legend('TL062', 'MC34074','NE5532', 'LM358', 'Ideal');
title('Different Op Amp performance');

% figure(2);
% hold on;
% grid on;
% plot(Vin, Vo_LM358_NL, 'r-');
% plot(Vin, Vo_LM358_L , 'c-');
% plot(Vin, Vo_ideal, 'k-');
% xlabel('Vin');
% ylabel('Vout');
% legend('No load', '10K\ohm Load', 'Ideal');
% title('LM358 no load and loaded performance');